# Documentation du modèle de données WDM

Walk Data Model (WDM) : modèle de données utilisé pour le projet, calqué sur le modèle OpenStreetMap (OSM) géométrique avec des ![node picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png) noeuds, des ![way picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_way.svg/20px-Osm_element_way.svg.png) chemins, des ![area picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_area.svg/20px-Osm_element_area.svg.png) zones et des ![relation picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_relation.svg/20px-Osm_element_relation.svg.png) relations, auxquels on ajoute des attributs clef=valeur (aussi appelés tags).


**Table des matières :**

[[_TOC_]]



# SitePathLink=*
On utilisera SitePathLink=* pour tracer tout type de voirie utilisable par un piéton.

S'utilise sur les ![way picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_way.svg/20px-Osm_element_way.svg.png) chemins, à l'exception des passages piétons (SitePathLink=Crossing) et des ascenseurs (SitePathLink=Elevator) qui peuvent être modélisés par un ![node picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png) noeud ou un ![way picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_way.svg/20px-Osm_element_way.svg.png) chemin.

Les noeuds d'origine et de destination doivent correspondre au début d'un autre SitePathLink ou être un entrée (Entrance=`*`), un équipement (Amenity=`*`) ou une place de parking (ParkingBay=`*`).

### Valeurs

| Clef         | Valeur          | Description                                                                                                         |
| ------------ | --------------- | ------------------------------------------------------------------------------------------------------------------- |
| SitePathLink | Pavement        | un trottoir                                                                                                         |
| SitePathLink | Footpath        | un chemin piéton, une voie cyclable partagée avec les piétons                                                       |
| SitePathLink | Street          | une rue piétonne, une voie de parking ou une impasse sans trottoir où les piétons sont tolérés                      |
| SitePathLink | OpenSpace       | une place piétonne                                                                                                  |
| SitePathLink | Concourse       | rue couverte, traboule                                                                                              |
| SitePathLink | Passage         | passage étroit, ruelle étroite                                                                                      |
| SitePathLink | Stairs          | un escalier                                                                                                         |
| SitePathLink | Elevator        | un ascenseur                                                                                                        |
| SitePathLink | Escalator       | un escalator                                                                                                        |
| SitePathLink | Travelator      | un tapis roulant                                                                                                    |
| SitePathLink | Ramp            | une rampe d'accès, structure en pente permettant de franchir une dénivellation, un changement de niveau ou d'étage. |
| SitePathLink | Crossing        | un passage piéton, un passage à niveau pour les piétons, ou une traversée                                           |
| SitePathLink | Hall            | un cheminement en intérieur, dans une salle ou un hall                                                              |
| SitePathLink | Corridor        | un cheminement en intérieur dans un couloir, etc                                                                    |
| SitePathLink | QueueManagement | un cheminement dédié à une file d'attente, la gestion de queue                                                      |
| SitePathLink | Quay            | un quai de tram, de bus, etc                                                                                        |



### Tags fréquents

| Clef                | Description                                                            | Contraintes sur la valeur                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | Exemple                           |
| ------------------- | ---------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------- |
| Name                | le nom si pertinent (par exemple pour les rues piétonnes)              |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | Name = Chemin Forestier des Dames |
| WheelchairAccess    | indique si l'access possible en fauteuil roulant                       | Valeurs autorisées : <br> - Yes (oui), <br> - No (non), <br>- Limited (partiellement, à préciser via le tag WheelchairAccess:Description)                                                                                                                                                                                                                                                                                                                                                                                                                                | WheelchairAccess = Yes            |
| Tilt                | Dévers en pourcentage ou en degré. TODO valeurs négatives autorisées ? | préciser l'unité (% ou deg) dans le champ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | Tilt = 2% <br> Tilt = 12deg       |
| Slope               | Pente en pourcentage ou en degré  TODO valeurs négatives autorisées ?  | préciser l'unité (% ou deg) dans le champ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | Slope = 2% <br> Slope = 12deg     |
| HighwayType         | type de voie d’après le schéma direction de la voirie français         | Valeurs autorisées : <br>- Street (voie classique : rue, avenue, boulevard) <br>- LimitedSpeedStreet (zone 30) <br>- LivingStreet (zone de rencontre) <br>- Pedestrian (rue piétonne, aire piétonne, sente piétonne) <br>- Bicycle (voie verte) <br>- Unclassified (autre type de voie inscrit au schéma directeur de la voirie)                                                                                                                                                                                                                                         | HighwayType=LivingStreet          |
| Width               | largeur utile de passage                                               | En mètre arrondi au mètre. </br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Width = 24                        |
| StructureType       | type de passage                                                        | Si non renseigné, on suppose que StructureType = Pathway.<br>Valeurs autorisées : <br> - Pathway (sentier) <br> - Overpass (passerelle, pont),  <br> - Underpass (passage sous-terrain) <br> - Tunnel (tunnel)<br> - Corridor (couloir)                                                                                                                                                                                                                                                                                                                                  | StructureType=Tunnel              |
| Outdoor             | couverture du cheminement                                              | Valeurs autorisées : <br> - Yes  (extérieur non couvert) <br> - No (intérieur) <br> - Covered (extérieur couvert)                                                                                                                                                                                                                                                                                                                                                                                                                                                        | Outdoor=No                        |
| Incline             | sens du déplacement, dans le sens du tracé                             | Valeurs autorisées : <br> - Up (ça monte)<br> - Down (ça descend)<br> -No (pas de changement de niveau)                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Incline=Down                      |
| FlooringMaterial    | matériau de revêtement au sol                                          | Valeurs autorisées : <br>- Asphalt (asphalte) <br>- Concrete (béton) <br>- Wood (bois) <br>- Dirt (terre) <br>- Gravel (graviers) <br>- Grass (gazon) <br>- CeramicTiles (carrelage) <br>- PlasticMatting (matière plastique) <br>- SteelPlate (plaques métalliques) <br>- Sand (sable stabilisé) <br>- Stone (pierre) <br>- Carpet (tapis) <br>- Rubber (caoutchouc) <br>- Cork (liège) <br>- FibreglassGrating (taillebotis en fibre de verre) <br>- GlazedCeramicTiles (carreaux de céramique émaillés) <br>- Vinyl (vinyle) <br>- Uneven (matériau inégal par nature) <br>- Other (autre matériau non listé ici) | FlooringMaterial=Asphalt          |
| Flooring            | état du revêtement                                                     | Valeurs autorisées : <br>- None (pas de revêtement) <br>- Good (bon état) <br>- Worn (dégradation sans gravité) <br>- Discomfortable (dégradation entraînant une difficulté d'usage ou d'inconfort) <br>- Hazardous (dégradation entraînant un problème de sécurité immédiat)                                                                                                                                                                                                                                                                                            | Flooring:Status = Worn            |
| TactileGuidingStrip | bande de guidage podotactile                                           | Valeurs autorisées : <br> - Yes (oui), <br> - No (non)                                                                                                                                                                                                                                                                                                                              |
| Lighting                     | éclairage                                                                                                                                                            | Valeurs autorisées : <br> - Yes (éclairé et adapté aux déficients visuels) <br> - No (pas d'éclairage) <br> - Bad (éclairé mais non adapté pour les déficients visuels)    | Lighting = Yes                                                                  |

#### Tags complémentaires des SitePathLink = Crossing


Les passages piétons et traversées peuvent avoir des tags complémentaires :
| Clef | Description | Contraintes sur la valeur | Exemple |
| ---- | ----------- | ------------------------- | ------- |
| Crossing | type de traversée | Valeurs autorisées : <br> - Rail (passage à niveau)<br> - UrbanRail (passage à niveau croisant des voies de tram)<br> -  Road (passage piéton, traversée de route) <br> -  CycleWay (passage piéton sur piste cyclable) | Crossing=Road |
| PedestrianLights | présence de feux lumineux | Valeurs autorisées : <br> - Yes <br> - No  | PedestrianLights=No |
| ZebraCrossing | présence et état du marquage au sol | Valeurs autorisées : <br>- None (pas de marquage au sol) <br>- Good (bon état) <br>- Worn (dégradation sans gravité) <br>- Discomfortable (dégradation entraînant une difficulté d'usage ou d'inconfort) <br>- Hazardous (dégradation entraînant un problème de sécurité immédiat) | ZebraCrossing = None |
| VisualGuidanceBands | présence de repère continu au sol |Valeurs autorisées : <br> - Yes <br> - No | VisualGuidanceBands=Yes  |
| CrossingIsland | présence d'un îlot de refuge dans la traversée |Valeurs autorisées : <br> - Yes <br> - No | CrossingIsland=Yes  |
| AcousticCrossingAids | présence et état de répétiteurs sonores ou d'aide acoustique à la traversée | Valeurs autorisées : <br>- None (pas de répétiteurs sonores) <br>- Good (bon état) <br>- Worn (dégradation sans gravité) <br>- Discomfortable (dégradation entraînant une difficulté d'usage ou d'inconfort) <br>- Hazardous (dégradation entraînant un problème de sécurité immédiat) | AcousticCrossingAids=Good |


**Remarque** : on peut en complément ajouter des tags sur les ![node picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png) noeuds extrémités PathJunction=Yes pour indiquer la hauteur du ressaut (KerbHeight) ou encore la présence de Bande d'Éveil de Vigilance (BEV) :
* ci-dessous, un passage piéton présentant un ressaut important :
![img_SitePathLink_Crossing_KerbHeight](img_SitePathLink_Crossing_KerbHeight.png)
* ci-dessous, un passage piéton avec un abaissement de trottoir :
![img_SitePathLink_Crossing_KerbCut](img_SitePathLink_Crossing_KerbCut.png)

#### Tags complémentaires des SitePathLink = Elevator

| Clef | Description | Contraintes sur la valeur |
| ---- | ----------- | ------------------------- |
| ElevatorType | type d'ascenseur | Valeurs autorisées : <br> - Lift (ascenseur) <br> - InclinedLift (ascenseur incliné, funiculaire). À utiliser uniquement sur les ![way picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_way.svg/20px-Osm_element_way.svg.png) chemins <br> - MovingPlatform (élévateur) |
| Depth | longueur de la cabine. Rappel : Width correspond à la largeur utile, donc la largeur de la porte |en mètres |
| InternalWidth | largeur de la cabine. Rappel : Width correspond à la largeur utile, donc la largeur de la porte |en mètres |
| RaisedButtons | Signale si les boutons de l'ascenseur sont en relief |Valeurs autorisées : <br> - None (aucune touche différenciée) <br> - Raised (touche différenciée par relief supérieur, au moins pour le rez-de-chaussée) <br> - Braille (touches en braille, avec la touche rez-de-chaussée différenciée par relief supérieur)  |
| AudioAnnouncements | Signale si l'ascenseur est équipé d'un mécanisme d'annonces sonores de l'étage |Valeurs autorisées : <br> - Yes <br> - No  |
| MagneticInductionLoop | Signale la présence d'un équipement boucle à induction magnétique (BIM) |Valeurs autorisées : <br> - Yes <br> - No  |
| MirrorOnOppositeSide | Signale la présence d’un miroir en face de l’ascenceur |Valeurs autorisées : <br> - Yes <br> - No  |
| AutomaticDoor | signale si la porte s'ouvre automatiquement |Valeurs autorisées : <br> - Yes <br> - No  |
| Attendant | signale s’il y a un préposé à l’ascenseur |Valeurs autorisées : <br> - Yes <br> - No (il peut être utilisé en autonomie) |
| MaxWeight | charge maximale admissible | en kilogrammes, arrondi au kilogramme |
| HandrailHeight | hauteur de la main courante | en mètres, arrondi au centimètre |


**Remarque** : trois modélisations sont possibles pour un ascenseur classique
<br>*la plus complète, pour les ascenseurs en intérieur*
![img_SitePathLink_Elevator_Area](img_SitePathLink_Elevator_Area.png)
* l'ascenseur est représenté par un SitePathLink=Elevator
* La cage d'ascenseur est représentée par un InsideSpace=Room.
* Les portes de l'ascenseur sont des Entrance=InsideSpace, positionnées à l'intersection entre le SitePathLink et l'InsideSpace.
* Les dimensions de la cabine sont indiquées sur le SitePathLink avec Depth et InternalWidth
* La largeur des portes est indiquées avec
    * Width sur chaque entrée
    * Width sur le SitePathLink

*représentation plus simple* :
<br>la cage d'ascenseur et les portes ne sont pas obligatoires.
<br>Dans ce cas, bien préciser
* la largeur de la porte avec Width
* les dimensions de la cabine avec Depth (qui doit correspondre environ à la longeur du SitePathLink) et InternalWidth
* s'il s'agit de portes automatiques avec AutomaticDoor
![img_SitePathLink_Elevator_Way](img_SitePathLink_Elevator_Way.png)


Représentation minimale :
<br>il est également possible de représenter l'ascenseur uniquement avec un point un SitePathLink existant. 
<br>C'est la représentation la plus adaptée lorsque pour le cas d'un ascenseur où les portes s'ouvrent toujours du même côté.
![img_SitePathLink_Elevator_Node](img_SitePathLink_Elevator_Node.png)


### Tags complémentaires des SitePathLink

Les tags suivants peuvent également être présents sur certains objets SitePathLink.

| Clef     | Description                                                                    | Contraintes sur la valeur                                                                                                                                  | SitePathLink concernés       |
| -------- | ------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------- |
| Handrail | présence d'une main courante sur laquelle on peut poser la main pour s'appuyer | Valeurs autorisées : <br> - Right (à droite uniquement)  <br> - Left (à gauche uniquement)  <br> - Both (des deux côtés) <br> - None (pas de main courante) | Ramp, Stairs, Elevator |
| StepCount | nombre de marches. dans le cas d'un escalier avec plusieurs volées de marches, c'est le nombre de marches total | Nombre entier positif | Stairs |
| StairFlightCount | nombre de volées de marches. <br>Si non renseigné, on suppose que StairFlightCount=0 | Nombre entier positif |  Stairs |
| StepColourContrast | état et présence de marches contrastées | Valeurs autorisées : <br>- None (pas de contraste) <br>- Good (bon état) <br>- Worn (dégradation sans gravité) <br>- Discomfortable (dégradation entraînant une difficulté d'usage ou d'inconfort) <br>- Hazardous (dégradation entraînant un problème de sécurité immédiat) | Stairs |
| StepHeight | hauteur de marche | en mètres arrondi au cm | Stairs |
| StoppedIfUnused| signale un tapis roulant ou escalator qui s'arrête lorsqu'il n'est pas utilisé, et doit être remis en marche par un détecteur automatique ou un bouton |  Valeurs autorisées : <br> - Yes <br> - No  | Escalator, Travelator |
| Conveying | indique la direction de déplacement d'un escalator ou tapis roulant par rapport au sens du tracé. Pour ces objets, si non renseigné, on suppose que Conveying=Forward | Valeurs autorisées : <br>- Forward (dans le sens du tracé) <br>- Backward (dans le sens opposé du tracé) <br>- Reversible (dans les deux sens) | Escalator, Travelator |
| Gated | indique s'il faut passer une porte pour emprunter ce cheminement | Si non renseigné, on suppose que Gated = No. <br> Valeurs autorisées : <br> - Yes (accès limité par une porte, etc) <br> - No (accès ouvert et libre) | principalement Hall |
|ManoeuvringDiameter |diamètre de la zone de manœuvre des usagers en fauteuil roulant. Pour les quais, mesuré une fois la palette ou la plate-forme élévatrice déployée | Décimal positif en mètre, arrondi au cm| Quay, Elevator |
| AudibleSignals | indique si une signalétique auditive est disponible |Valeurs autorisées : <br> - Yes (oui), <br> - No (non), <br>- Limited (partiellement, à préciser via le tag AudibleSignals:Description) | |
| VisualSigns | indique si une signalétique visuelle est disponible |Valeurs autorisées : <br> - Yes (oui), <br> - No (non), <br>- Limited (partiellement, à préciser via le tag VisualSigns:Description) | |
| TransportMode | le mode de transport en commun | Valeurs autorisées : <br> - Bus <br> - Coach (car)  <br> - Tram (tramway) <br> - Water (ferry, bateau bus) <br> - Funicular, TrolleyBus, CableWay | Quay uniquement |
| PublicCode | un identifiant public local, par exemple un numéro de quai dans une gare routière |  | Quay uniquement |
| TactileWarningStrip | Présence et état de la bande d'éveil vigilance (surface contrastée visuellement et tactilement permettant de signaler un danger) |Valeurs autorisées : <br>- None (pas de BEV) <br>- Good (bon état) <br>- Worn (dégradation sans gravité) <br>- Discomfortable (dégradation entraînant une difficulté d'usage ou d'inconfort) <br>- Hazardous (dégradation entraînant un problème de sécurité immédiat) | Quay uniquement |

# PointOfInterest = *

On utilisera PointOfInterest=* pour des points d'intérêts, tels que des commerces ou des gares.

S'utilise sur les ![area picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_area.svg/20px-Osm_element_area.svg.png) zones uniquement.

On tracera le contour de manière à englober tous les espaces intérieurs et extérieurs du lieu.

Chaque PointOfInterest devra avoir au moins une entrée, et pourra contenir des cheminements intérieurs (SitePathLink=Hall/Corridor) et des espaces intérieurs (InsideSpace=*)

### Valeurs


| Clef            | Valeur                  | Description                                                                   |
| --------------- | ----------------------- | ----------------------------------------------------------------------------- |
| PointOfInterest | Shop                    | Commerce, Magasins                                                            |
| PointOfInterest | Government              | Mairie, préfecture, service public, lieu administratif                        |
| PointOfInterest | Restaurant              | Restaurant, bar, café                                                         |
| PointOfInterest | Lodging                 | Hôtels                                                                        |
| PointOfInterest | Youth                   | École, crèche, centre de loisirs                                              |
| PointOfInterest | Library                 | Bibliothèque                                                                  |
| PointOfInterest | Healthcare              | Clinique, hôpital                                                             |
| PointOfInterest | Religion                | Mosquée, église                                                               |
| PointOfInterest | Bank                    | Banque                                                                        |
| PointOfInterest | Office                  | Bureaux                                                                       |
| PointOfInterest | Sport                   | Piscine couverte, salle polyvalente, gymnase, etc (établissemnt sportif clos) |
| PointOfInterest | Museum                  | Musée                                                                         |
| PointOfInterest | Game                    | Salle de danse et salle de jeux                                               |
| PointOfInterest | Exhibition              | Salle d’exposition                                                            |
| PointOfInterest | DisabledCare            | Structure d’accueil pour personnes handicapées                                |
| PointOfInterest | Senior                  | Maison de retraite                                                            |
| PointOfInterest | Theater                 | Théâtre, salles d'audition, salle de spectacles                               |
| PointOfInterest | OutdoorVenue            | Terrain de sport, stade, hippodrome                                           |
| PointOfInterest | StopPlace               | Gare, station                                                                 |
| PointOfInterest | ParkingLots             | Parc de stationnement couvert                                                 |
| PointOfInterest | MountainShelter         | Refuge de montagne  |
| PointOfInterest | AltitudeHotelRestaurant | Restaurant ou hôtel d’altitude | OA : Hôtels-restaurants d’Altitude |
| PointOfInterest | InflatableVenue | Château gonflable | SG : Structures Gonflables |

En complément, on pourra trouver un second tag précisant le type de point d'intérêt et reprenant pour clef la valeur du tag PointOfInterest : par exemple PointOfInterest=Government + Government=Townhall. La liste se trouve ci-dessous.

#### StopPlace = *

| Clef | Valeur | Description|
| ---- | ------ | ---------- |
| StopPlace | RailStation | Gare, station ferrée |
| StopPlace | MetroStation| Station de métro |
| StopPlace | BusStation | Gare routière |
| StopPlace | CoachStation | Gare routière de cars (longue distance, scolaire, etc). À utiliser uniquement s'il n'y a que des cars (sinon, préférer BusStation) |
| StopPlace | TramStation | Station de tramway. À utiliser uniquement pour une station séparée physiquement de la chaussée (ne pas tracer de StopPlace dans le cas de quais directement sur le trottoir) |
| StopPlace | FerryPort | Port de ferry, station de bateau-bus |
| StopPlace | LiftStation | Station de téléphérique |
| StopPlace | Airport | Aéroport|

Remarque : on pourra également trouver des StopPlace=OnstreetBus ou OnstreetTram sur des ![relation picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_relation.svg/20px-Osm_element_relation.svg.png) relations. Il s'agit d'objets abstraits importés lors d'un import Netex et qu'on souhaite conserver lors de l'export Netex. Ils n'ont pas de tag PointOfInterest=StopPlace et il n'est pas nécessaire de renseigner d'information dessus.

#### Shop = *

| Clef   | Valeur             | Description                                |
| :---- | :----------------- | :----------------------------------------- |
| Shop  | Alcohol            | Magasin de vente d'alcool                  |
| Shop  | Antiques           | Antiquaire                                 |
| Shop  | Art                | Marchand d'art                             |
| Shop  | Bag                | Bagagerie                                  |
| Shop  | Bakery             | Boulangerie                                |
| Shop  | Beauty             | Salon de beauté                            |
| Shop  | Bed                | Magasin de literie                         |
| Shop  | Beverages          | Magasin de boissons                        |
| Shop  | Bicycle            | Magasin de vélos                           |
| Shop  | BoatRental         | Location de bateau                         |
| Shop  | Books              | Librairie                                  |
| Shop  | BureauDeChange     | Bureau de change                           |
| Shop  | Butcher            | Boucherie                                  |
| Shop  | Car                | Concessionnaire automobile                 |
| Shop  | CarParts           | Magasin de pièces automobiles              |
| Shop  | Carpet             | Magasin de tapis                           |
| Shop  | CarRental          | Location de voiture                        |
| Shop  | CarRepair          | Garage de réparation automobile            |
| Shop  | CarWash            | Station de lavage de voiture               |
| Shop  | Cheese             | Fromagerie                                 |
| Shop  | Chemist            | Parapharmacie                              |
| Shop  | Chocolate          | Chocolatier                                |
| Shop  | Clothes            | Magasin de vêtements                       |
| Shop  | Coffee             | Boutique de vente de cafés                 |
| Shop  | Computer           | Magasin d'informatique                     |
| Shop  | Confectionery      | Confiserie                                 |
| Shop  | Convenience        | Épicerie / Supérette                       |
| Shop  | Copyshop           | Photocopie et impression                   |
| Shop  | Cosmetics          | Magasin de cosmétiques                     |
| Shop  | Crematorium        | Crématorium                                |
| Shop  | Deli               | Épicerie fine                              |
| Shop  | DepartmentStore    | Grand magasin                              |
| Shop  | DoItYourself       | Magasin de bricolage                       |
| Shop  | DrivingSchool      | Auto-école                                 |
| Shop  | DryCleaning        | Pressing                                   |
| Shop  | Ecigarette         | Magasin de cigarettes électroniques        |
| Shop  | EstateAgent        | Agence immobilière                         |
| Shop  | Fabric             | Magasin de tissus                          |
| Shop  | Farm               | Magasin de producteurs                     |
| Shop  | Florist            | Fleuriste                                  |
| Shop  | FrozenFood         | Magasin de produits surgelés               |
| Shop  | Fuel               | Station-service                            |
| Shop  | FuneralDirectors   | Pompes funèbres                            |
| Shop  | Furniture          | Magasin de meubles                         |
| Shop  | Games              | Magasin de jeux de plateau                 |
| Shop  | GardenCentre       | Jardinerie                                 |
| Shop  | Gift               | Boutique de cadeaux                        |
| Shop  | Greengrocer        | Marchand de fruits et légumes              |
| Shop  | Guide              | Bureau des guides                          |
| Shop  | Hairdresser        | Salon de coiffure                          |
| Shop  | Hardware           | Quincaillerie                              |
| Shop  | HearingAids        | Audioprothésiste                           |
| Shop  | Hifi               | Magasin de matériel hi-fi                  |
| Shop  | Insurance          | Agence d'assurance                         |
| Shop  | InteriorDecoration | Magasin de décoration d'intérieur          |
| Shop  | InternetCafe       | Cybercafé                                  |
| Shop  | Jewelry            | Bijouterie                                 |
| Shop  | Kiosk              | Kiosque                                    |
| Shop  | Kitchen            | Cuisiniste                                 |
| Shop  | Laundry            | Laverie                                    |
| Shop  | Leather            | Maroquinerie                               |
| Shop  | Massage            | Salon de massage                           |
| Shop  | MedicalSupply      | Magasin de matériel médical                |
| Shop  | MobilePhone        | Magasin de téléphonie mobile               |
| Shop  | Motorcycle         | Magasin de motos                           |
| Shop  | MotorcycleRepair   | Réparateur de motos                        |
| Shop  | MovingCompany      | Entreprise de déménagement                 |
| Shop  | Music              | Magasin de musique                         |
| Shop  | Newsagent          | Kiosque à journaux                         |
| Shop  | Nightclub          | Boîte de nuit                              |
| Shop  | Optician           | Opticien                                   |
| Shop  | Outdoor            | Magasin de matériel de sports de plein air |
| Shop  | Pastry             | Pâtisserie                                 |
| Shop  | Perfumery          | Parfumerie                                 |
| Shop  | Pet                | Animalerie                                 |
| Shop  | Pharmacy           | Pharmacie                                  |
| Shop  | Seafood            | Poissonnerie / Vente de fruits de mer      |
| Shop  | SecondHand         | Magasin de produits d'occasion             |
| Shop  | Sewing             | Magasin de couture                         |
| Shop  | Shoes              | Magasin de chaussures                      |
| Shop  | Sports             | Magasin d'équipement sportif               |
| Shop  | Stationery         | Papeterie                                  |
| Shop  | Supermarket        | Supermarché                                |
| Shop  | Tattoo             | Salon de tatouage                          |
| Shop  | Tea                | Magasin de thés                            |
| Shop  | Ticket             | Boutique de vente de billets               |
| Shop  | Tobacco            | Bureau de tabac                            |
| Shop  | Toys               | Magasin de jouets                          |
| Shop  | TravelAgency       | Agence de voyages                          |
| Shop  | Variety_Store      | Magasin discount                           |
| Shop  | VehicleInspection  | Centre de contrôle technique               |
| Shop  | VideoGames         | Magasin de location et vente de jeux vidéo |
| Shop  | Watches            | Magasin de montres                         |

#### Government = *

| Clef        | Valeur                | Description                     |
|:---------- |:--------------------- |:------------------------------- |
| Government | Audit                 | Cour des comptes                |
| Government | ChamberOfCommerce     | Chambre de commerce             |
| Government | CourtHouse            | Tribunal                        |
| Government | Customs               | Douane                          |
| Government | GeneralDesk           | Maison des services publics     |
| Government | LocalAuthority        | Bureau de communauté d’agglo    |
| Government | Parliament            | Conseil régional, départemental |
| Government | Police                | Police, Gendarmerie             |
| Government | Post                  | Bureau de Poste                 |
| Government | Prefecture            | Préfecture                      |
| Government | SocialSecurity        | CPAM                            |
| Government | SocialWelfare         | Caf                             |
| Government | Tax                   | Centre des impôts, Urssaf       |
| Government | Townhall              | Mairie, hôtel de ville          |
| Government | UnemploymentInsurance | Pôle Emploi                     |

#### Restaurant = *

| Clef        | Valeur     | Description                      |
| :--------- | :--------- | :------------------------------- |
| Restaurant | Cafe       | Café                             |
| Restaurant | FastFood   | Restauration rapide              |
| Restaurant | FoodCourt  | Aire de restauration, food-court |
| Restaurant | IceCream   | Magasin de crèmes glacées        |
| Restaurant | Pub        | Pub                              |
| Restaurant | Restaurant | Restaurant                       |

#### Education = *

| Clef       | Valeur           | Description                                            |
|:--------- |:---------------- |:------------------------------------------------------ |
| Education | Kindergarten     | Crèche, garderie                                       |
| Education | EarlyChildhood   | École maternelle                                       |
| Education | Elementary       | Enseignement élémentaire                               |
| Education | Secondary        | Enseignement secondaire                                |
| Education | Tertiary         | Enseignement tertiaire, professionnel ou technologique |
| Education | Primary          | École primaire                                         |
| Education | MiddleSchool     | Collège                                                |
| Education | HighSchool       | Lycée                                                  |
| Education | College          | Enseignement supérieur non universitaire               |
| Education | RecreationCenter | Centre de loisir                                       |
| Education | University       | Université, enseignement supérieur universitaire       |


#### Theater = *

| Clef     | Valeur  | Description |
| :------ | :------ | :---------- |
| Theater | Cinema  | Cinéma      |
| Theater | Theater | Théâtre     |

#### Religion = *

| Clef      | Valeur    | Description           |
| :------- | :-------- | :-------------------- |
| Religion | Buddhist  | Temple boudhiste      |
| Religion | Christian | Église chrétienne     |
| Religion | Hindu     | Temple hindou         |
| Religion | Jewish    | Synagogue             |
| Religion | Muslim    | Mosquée               |
| Religion | Shinto    | Sanctuaire shintoïste |
| Religion | Sikh      | Temple Sikh           |
| Religion | Taoist    | Temple Taoïste        |

#### Healthcare = *

| Clef        | Valeur     | Description                     |
|:---------- |:---------- |:------------------------------- |
| Healthcare | Clinic     | Clinique                        |
| Healthcare | Doctor     | Cabinet médical                 |
| Healthcare | Hospital   | Hôpital                         |
| Healthcare | Laboratory | Laboratoire d'analyses médicale |

#### Game = *

| Clef  | Valeur            | Description        |
|:---- |:----------------- |:------------------ |
| Game | AdultGamingCenter | Jeux pour adultes  |
| Game | ArcadeCenter      | Salle de jeux      |
| Game | EscapeRoom        | Escape game        |
| Game | Hackerspace       | Tiers-lieu         |
| Game | Resort            | Parc d'attractions |

#### Sport = *

| Clef   | Valeur            | Description             |
| :---- | :---------------- | :---------------------- |
| Sport | BowlingCenter     | Bowling                 |
| Sport | ClimbingCenter    | Salle d'escalade        |
| Sport | DancingCenter     | Salle de danse          |
| Sport | FitnessCenter     | Salle de fitness / gym  |
| Sport | GolfingCenter     | Parcours de golf        |
| Sport | HorseRidingCenter | Centre équestre         |
| Sport | IceCenter         | Patinoire               |
| Sport | SportCenter       | Centre sportif          |
| Sport | Stadium           | Stade                   |
| Sport | SwimmingCenter    | Piscine                 |

### Tags fréquents

| Clef             | Description                                                            | Contraintes sur la valeur                                                                                                                                               | PointOfInterest concernés        |
| ---------------- | ---------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------- |
| Name             | nom du lieu                                                            | exemple : Name=Gare Routière de Torcy                                                                                                                                   |                                  |
| WheelchairAccess | indique si l'accès est possible en fauteuil roulant                    | Valeurs autorisées : <br> - Yes (oui), <br> - No (non), <br>- Limited (partiellement, à préciser via le tag WheelchairAccess:Description)                               |                                  |
| AudibleSignals   | indique si une signalétique auditive est disponible                    | Valeurs autorisées : <br> - Yes (oui), <br> - No (non), <br>- Limited (partiellement, à préciser via le tag AudibleSignals:Description)                                 |   Pour les PointOfInterest=StopPlace, privilégier AudioInformation                               |
| AudioInformation | indique si une information audio est disponible | Valeurs autorisées : <br> - Yes (oui et adaptée pour les malentendants) <br> - No (non) <br>- Bad (oui mais non adaptée pour les malentendants) | PointOfInterest=StopPlace |
| VisualSigns      | indique si une signalétique visuelle est disponible                    | Valeurs autorisées : <br> - Yes (oui), <br> - No (non), <br>- Limited (partiellement, à préciser via le tag VisualSigns:Description)                                    |                                  |
| VisualDisplays | indique la présence d'écran d’affichage | Valeurs autorisées : <br> - Yes (oui et adaptés pour les mal voyants) <br> - No (non) <br>- Bad (oui mais non adaptée pour les mal voyants) | PointOfInterest=StopPlace |
| Image            | image, picto ou photo du lieu                                          | URL valide                                                                                                                                                              |                                  |
| Phone            | numéro de téléphone                                                    | au format international                                                                                                                                                 |                                  |
| Website          | site internet                                                          | URL valide                                                                                                                                                              |                                  |
| WiFi | indique si du wifi est disponible | Valeurs autorisées : <br> - Free (wifi gratuit disponible) <br> - Yes (wifi disponible) <br>- No (pas de wifi) | |
| `Ref:FR:SIRET`   | Le numéro SIRET de l'aménageur issue de la base SIRENE des entreprises | Séquence de 14 chiffres, correspondant à un élément valide dans la base SIRENE de l'Insee                                                                               |                                  |
| AddressLine      | adresse postale                                                        | numéro et nom de rue                                                                                                                                                    |                                  |
| PostCode         | code postal                                                            | Séquence de 5 chiffres, correspondant à un code postal valide                                                                                                           |                                  |
| Lighting         | éclairage                                                              | Valeurs autorisées : <br> - Yes (éclairé et adapté aux déficients visuels) <br> - No (pas d'éclairage) <br> - Bad (éclairé mais non adapté pour les déficients visuels) |                                  |
| NearbyCarPark | indique la présence de parking à proximité. *Les places PMR sont à positionner et qualifier avec ParkingBay=Disabled* | Valeurs autorisées : <br> - Yes (oui) <br> - No (non) | |
| NearbyCyclePark | indique la présence de parking à vélo à proximité | Valeurs autorisées : <br> - Yes (oui) <br> - No (non) | |
| Toilets | indique si des toilettes sont disponibles. *À n'utiliser que lorsqu'on ne peut pas positionner et qualifier plus précisément un Amenity=Toilets* | Valeurs autorisées : <br> - Yes (oui et accessible pour les fauteuils roulants) <br> - No (non) <br>- Bad (oui mais non accessible pour les fauteuils roulants) | |
| Toilets:Shower | indique si une douche ou un espace pour se laver et se changer est disponible dans les toilettes. *À n'utiliser que lorsqu'on ne peut pas positionner et qualifier plus précisément un Amenity=Toilets* | Valeurs autorisées : <br> - Yes (oui) <br> - No (non) | |
| Toilets:BabyChange | indique si un espace bébé est disponible dans les toilettes. *À n'utiliser que lorsqu'on ne peut pas positionner et qualifier plus précisément un Amenity=Toilets* | Valeurs autorisées : <br> - Yes (oui et accessible pour les fauteuils roulants) <br> - No (non) <br>- Bad (oui mais non accessible pour les fauteuils roulants) | |
| Tickets | services billetique disponibles. *À compléter avec des Amenity=TicketVendingMachine/TicketOffice* | Valeurs autorisées :  une liste d'éléments parmi la liste ci-dessous, avec un séparateur ";" <br> - VendingMachine (machine de vente de billet) <br> - Office (guichet de vente de billet) <br>- OnDemandMachines (machine d’impression de billet acheté en ligne <br>- MobileTicketing (billettique mobile – sur smartphones) | PointOfInterest=StopPlace/Theater/Game/Sport |
| Staffing | indique la présence de personnel. *À n'utiliser que lorsqu'on ne peut pas positionner et qualifier plus précisément un Amenity=ReceptionDesk* | Valeurs autorisées : <br> - FullTime (personnel présent en permanence) <br> - No (sans personnel) <br>- PartTime (personnel présent à temps partiel) | |
| LargePrintTimetables | indique la présence de grand panneau d’affichage des horaires | Valeurs autorisées : <br> - Yes (oui) <br> - No (non) | PointOfInterest=StopPlace |
| RealTimeDepartures | indique si les horaires de départ temps-réel sont disponibles | Valeurs autorisées : <br> - Yes (oui) <br> - No (non) | PointOfInterest=StopPlace |
| Assistance | services d'assistance disponibles | Valeurs autorisées : <br> None (aucun service d'assistance) ou une liste d'éléments parmi la liste ci-dessous, avec un séparateur ";"  <br> - Boarding (assistance à l’embarquement) <br> - Wheelchair (assistance pour les fauteils roulants) <br>- UnaccompaniedMinor (assistance pour les mineurs non accompagnés) <br>- Conductor (chef de gare disponible) <br>- Information (guichet d'information disponible. Dans ce cas, positionner un Amenity=ReceptionDesk également) | PointOfInterest=StopPlace |
| AccessibilityToolLending | services de mise à disposition d'équipements d'aide à l'accessibilité disponibles  | Valeurs autorisées : une liste d'éléments parmi la liste ci-dessous, avec un séparateur ";" <br> - Wheelchair (fauteils roulants disponibles) <br> - WalkingStick (cannes disponibles) <br>- AudioNavigator (navigateurs audios disponibles) <br> - VisualNavigator (navigateurs visuels disponibles) <br> - PassengerCart (caddies disponibles) <br>- Pushchair (poussettes disponibles) <br>- Umbrella (parapluies disponibles) | |
| FamilyServices | services disponibles pour la famille | Valeurs autorisées : <br> None (aucun service) ou une liste d'éléments parmi la liste ci-dessous, avec un séparateur ";"  <br> - Children (services et activités pour les enfants) <br> - Nursery (service de garderie) | |
| Emergency | services d'urgence disponibles | Valeurs autorisées :  une liste d'éléments parmi la liste ci-dessous, avec un séparateur ";" <br> - Police (police) <br> - Fire (incendie) <br>- FirstAid (premiers secours) <br>- SOSPoint (point SOS, appel d’urgence) | |
| Outdoor          | couverture du lieu                                                     | Valeurs autorisées : <br> - Yes  (extérieur non couvert) <br> - No (intérieur) <br> - Covered (extérieur couvert)                                                       | StopPlace=BusStation par exemple |
| Gated            | indique s'il faut passer une porte pour accéder à ce point d'intérêt   | <br> Valeurs autorisées : <br> - Yes <br> - No                                                                                                                          |                                  |

# Obstacle=*

Le tag Obstacle=* est utilisé sur un ![node picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png) noeud pour représenter un obstacle au cheminement, qui peut gêner ou empecher la circulation.

Le ![node picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png) noeud doit se trouver sur un ![way picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_way.svg/20px-Osm_element_way.svg.png) chemin SitePathLink.

Seuls les obstacles qui créent une gêne à la circulation doivent être cartographiés. Si la longueur de l'obstacle est supérieure à 3 mètres, on fera un SitePathLink dédié plutôt qu'on obstacle pour renseigner plus en détail les conditions de passage.

| Clef | Valeur | Description|
| ---- | ------ | ---------- |
| Obstacle | Kerb | ressaut ou marche |
| Obstacle | Bench | banc |
| Obstacle | Shelter | abri, abribus |
| Obstacle | Toilets | toilettes |
| Obstacle | Pole | poteau |
| Obstacle | PostBox | boîte aux lettres |
| Obstacle | Vegetation | arbre, haie |
| Obstacle | Turnstile | tourniquet |
| Obstacle | Bollard | poteau, borne |
| Obstacle | CycleBarrier | barrière sur la voie destinée à ralentir voire interdire l'accès aux vélos et engins motorisés |
| Obstacle | RoughSurface | surface irrégulière |
| Obstacle | Yes | autre type d'obstacle non présent dans cette liste |

## Tags fréquents

| Clef | Description | Contraintes sur la valeur |
| -------- | -------- | -------- |
| ObstacleType | Type d'obstacle | Valeurs autorisées : <br> - Surface (obstacle en surface que l'on doit contourner) <br> - Ground (obstacle posé au sol que l'on doit contourner) <br> - Overhanging (obstacle en saillie, on peut le contourner ou passer en dessous) <br> - Pass (un passage sélectif, il faut passer au travers) |
| Width | largeur de l'obstacle (attention, il ne s'agit pas de la largeur de passage utile mais bien de la largeur de l'ostacle) | En mètres |
| Height | hauteur entre le sol et le haut de l'obstacle dans le cas d'un obstacle posé au sol, ou entre le sol et le bas de l'obstacle dans le cas d'un obstacle en saillie | En mètres |
| Length | longueur de l'obstacle. Si plus de 3 mètres, on tracera un SitePathLink dédié plutôt qu'on obstacle ponctuel | En mètres |
| Image | image, picto ou photo de l'obstacle | URL valide |
| FlooringMaterial | matériau de revêtement au sol (pour les Obstacle=RoughSurface) | Valeurs autorisées : <br> - Asphalt (asphalte) - Bricks (brique) <br> - Cobbles (pavés) <br> - Earth (terre) <br> - Grass (gazon) <br> - LooseSurface (surface instable) <br> - PavingStones (pierres) <br> - Smooth (lisse) <br> - Other (autre)|

# Amenity=*

Le tag Amenity=* est utilisé sur un ![node picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png) noeud pour représenter un équipement ou un aménagement.

Si cet équipement constitue également un obstacle à la circulation (par exemple un banc qui réduit l'espace disponible sur le trottoir) il doit également (ou en remplacement) avoir un tag Obstacle=*.

S'il s'agit un équipement intérieur, il doit se situer à l'intérieur de la ![area picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_area.svg/20px-Osm_element_area.svg.png) zone  InsideSpace ou PointOfInterest qui le contient.

| Clef | Valeur | Description|
| ---- | ------ | ---------- |
| Amenity | ReceptionDesk | accueil, guichet d'un ERP |
| Amenity | TicketOffice | guichet permettant d'acheter des tickets. À utiliser dans les PointOfInterest=StopPlace/Theater/Game/Sport |
| Amenity | Shelter | abri, abribus |
| Amenity | Toilets | sanitaires (peut aussi s'utiliser pour une douche ou un espace pour changer un bébé) |
| Amenity | EmergencyPhone | téléphone d'urgence. À utiliser dans les PointOfInterest=StopPlace |
| Amenity | RubbishDisposal | poubelle ou ensemble de poubelles |
| Amenity | LuggageLocker | casier à bagages, consigne |
| Amenity | TrolleyStand | stand de charriots |
| Amenity | Seating | banc ou ensemble de bancs |
| Amenity | Sign | panneau d'affichage, signalétique |
| Amenity | TicketValidator | validateur pour titre de transport. S'il s'agit d'une ligne de tourniquets, ajouter Obstacle=Turnstile et Entrance=* parce que c'est l'entrée d'une autre zone intérieure |
| Amenity | TicketVendingMachine | automate de vente de titre de transport |
| Amenity | PostBox | boîte aux lettres |
| Amenity | Defibrillator | défibrillateur automatique externe (DAE) |
| Amenity | MeetingPoint | point rencontre, point de rendez-vous|

### Tags fréquents

| Clef                      | Description                                                                                      | Contraintes sur la valeur                                                                                                                                       | Amenity concernés                          |
| ------------------------- | ------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------ |
| WheelchairAccess          | indique si l'accès est possible en fauteuil roulant                                              | Valeurs autorisées : <br> - Yes (oui), <br> - No (non), <br>- Limited (partiellement, à préciser via le tag WheelchairAccess:Description)                       |                                            |
| AudibleSignals            | indique si une signalétique auditive est disponible                                              | Valeurs autorisées : <br> - Yes (oui), <br> - No (non), <br>- Limited (partiellement, à préciser via le tag AudibleSignals:Description)                         |                                            |
| VisualSigns               | indique si une signalétique visuelle est disponible                                              | Valeurs autorisées : <br> - Yes (oui), <br> - No (non), <br>- Limited (partiellement, à préciser via le tag VisualSigns:Description)                            |                                            |
| ManoeuvringDiameter       | rayon de braquage pour les usagers en fauteuil roulant (pour les Amenity=Toilets)                | Décimal positif en mètre, arrondi au cm                                                                                                                         | Amenity=Toilets                            |
| Toilets:Shower            | indique si une douche ou un espace pour se laver et se changer est disponible dans les toilettes | Valeurs autorisées : <br> - Yes (oui) <br> - No (non)                                                                                                           | Amenity=Toilets                            |
| Toilets:BabyChange        | indique si un espace bébé est disponible dans les toilettes                                      | Valeurs autorisées : <br> - Yes (oui et accessible pour les fauteuils roulants) <br> - No (non) <br>- Bad (oui mais non accessible pour les fauteuils roulants) | Amenity=Toilets                            |
| Staffing                  | indique la présence de personnel                                                                 | Valeurs autorisées : <br> - FullTime (personnel present en permanence) <br> - No (sans personnel) <br>- PartTime (personnel present à temps partiel)            | Amenity=Toilets/ReceptionDesk/TicketOffice |
| AccessibilityTrainedStaff | indique si le personnel a été formé à la prise en charge des problèmatiques d’accessibilité      | Valeurs autorisées : <br> - Yes (oui) <br> - No (non)                                                                                                           | Amenity=ReceptionDesk/TicketOffice         |
| FreeToUse                 | indique si l'équipement peut être utilisé gratuitement                                           | Valeurs autorisées : <br> - Yes (oui) <br> - No (non)                                                                                                           | Amenity=Toilets/TrolleyStand               |

# Entrance=*

Le tag Entrance=* est utilisé sur un ![node picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png) noeud pour représenter une entrée. Il peut s'agir d'une entrée de batiment, mais aussi de l'entrée d'un point d'intérêt (par exemple une bouche de métro) ou encore de l'entrée d'un lieu intérieur.

Le ![node picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png) noeud doit se trouver sur la ![area picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_area.svg/20px-Osm_element_area.svg.png) zone dont il est l'entrée (un POI ou un InsideSpace) et peut être relié à un ![way picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_way.svg/20px-Osm_element_way.svg.png) chemin SitePathLink.

Seules les entrées accessibles au public sont concernées (on ne cartographie pas les entrées de service, les entrées pour le personnel, etc).

| Clef     | Valeur          | Description                                                                                |
| -------- | --------------- | ------------------------------------------------------------------------------------------ |
| Entrance | PointOfInterest | Entrée d'un point d'intérêt. Pour les gares, utiliser plutôt StopPlace                     |
| Entrance | StopPlace       | Bouche de métro, entrée de gare                                                            |
| Entrance | Building        | Entrée de bâtiment. S'il s'agit aussi de l'entrée du lieu, utiliser plutôt PointOfInterest |
| Entrance | InsideSpace     | Entrée d'une pièce ou d'un espace intérieur                                                |

## Tags fréquents

| Clef | Description | Contraintes sur la valeur |
| -------- | -------- | -------- |
| EntrancePassage | Type d'accès | Valeurs autorisées : <br> - Opening (pas de porte, accès libre) <br> - Door (porte) <br> - Gate (portail) <br> - Barrier (barrière) |
| IsEntry | indique s'il s'agit d'une entrée (par opposition à une sortie) |Valeur par défaut : Yes. <br>Valeurs autorisées : <br> - Yes (on peut entrer ici) <br> - No (on ne peut pas entrer ici) |
| IsExit | indique s'il s'agit d'une sortie (par opposition à une entrée) |Valeur par défaut : Yes. <br>Valeurs autorisées : <br> - Yes (on peut sortir ici) <br> - No (on ne peut pas sortir ici) |
| PublicCode | un identifiant public local, par exemple un numéro de sortie pour les bouches de métro |  |
| Name | un nom public local, par exemple un nom de sortie pour les bouches de métro |  |
| WheelchairAccess | indique si l'accès est possible en fauteuil roulant | Valeurs autorisées : <br> - Yes (oui), <br> - No (non), <br>- Limited (partiellement, à préciser via le tag WheelchairAccess:Description) | 
| Width | largeur utile de passage | En mètres |
| Height | hauteur | En mètres |
| Door | caractéristiques de la porte principale de l'entrée |À ne renseigner que s'il y a une porte.<br>Valeurs autorisées : <br> - Hinged (porte classique)<br> - Sliding (coulissante)<br> - Revolving (Porte à tambour)<br> - Swing (Porte battante qui peut s'ouvrir dans les deux sens) <br> - Other (autre type de porte) |
| AutomaticDoor | indique si la porte est automatique |À ne renseigner que s'il y a une porte.<br>Valeurs autorisées : <br> - Yes <br> - No (il s'agit d'une porte manuelle) |
| DoorHandle | caractéristiques de la poignée de porte |À ne renseigner que s'il y a une porte.<br>Valeurs autorisées : <br> - None (pas de poignée) <br> - Lever (poignée à levier)<br> - Button (bouton)<br> - etc TODO |
| KerbHeight | Hauteur du ressaut, à mesurer entre le haut de la bordure du trottoir et la chaussée | en mètres |
| TactileWarningStrip | Présence et état de la bande d'éveil vigilance (surface contrastée visuellement et tactilement permettant de signaler un danger) |Valeurs autorisées : <br>- None (pas de BEV) <br>- Good (bon état) <br>- Worn (dégradation sans gravité) <br>- Discomfortable (dégradation entraînant une difficulté d'usage ou d'inconfort) <br>- Hazardous (dégradation entraînant un problème de sécurité immédiat)  |
| EntranceAttention | dispositif de contrôle d’accès à l’entrée | Valeurs autorisées : <br>- None (aucun dispositif) <br>- Doorbell (sonnette, bouton d'appel) <br>- Intercom (interphone) <br>- VideoIntercom (visiophone) <br>- Other (autre)  |
| NecessaryForceToOpen | la force nécessaire pour ouvrir la porte |À ne renseigner que s'il y a une porte.<br>En Newton |

# ParkingBay=*

Le tag ParkingBay=* est utilisé sur un ![node picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png) noeud pour représenter un place de parking.

Un ParkingBay doit être relié au reste du cheminement piéton par un SitePathLink.

| Clef | Valeur | Description|
| ---- | ------ | ---------- |
| ParkingBay | Disabled | une place de stationnement de voiture réservée aux personnes à mobilité réduite |

## Tags fréquents

| Clef | Description | Contraintes sur la valeur |
| -------- | -------- | -------- |
| BayGeometry | Type de stationnement | Valeurs autorisées : <br> - Orthogonal (perpendiculaire) <br> - Angled (en épis) <br> - Parallel (longitudinal) <br> - FreeFormat (libre) <br> - Unspecified (non précisé) |
| ParkingVisibility | Signalisation ou marquage au sol indiquant la spécificité du stationnement | Valeurs autorisées : <br> - Unmarked (non marqué) <br> -  SignageOnly (signalisation par panneau ou signalétique mais pas de marquage au sol) <br> -  MarkingOnly (marquage au sol uniquement) <br> -  Docks (démarquation physique) <br> -  Demarcated (marquage au sol et panneau) |
| FlooringMaterial | matériau de revêtement au sol | Valeurs autorisées : <br>- Asphalt (asphalte) <br>- Concrete (béton) <br>- Wood (bois) <br>- Dirt (terre) <br>- Gravel (graviers) <br>- Grass (gazon) <br>- CeramicTiles (carrelage) <br>- PlasticMatting (matière plastique) <br>- SteelPlate (plaques métalliques) <br>- Sand (sable stabilisé) <br>- Stone (pierre) <br>- Carpet (tapis) <br>- Rubber (caoutchouc) <br>- Cork (liège) <br>- FibreglassGrating (taillebotis en fibre de verre) <br>- GlazedCeramicTiles (carreaux de céramique émaillés) <br>- Vinyl (vinyle) <br>- Uneven (matériau inégal par nature) |
| Flooring | état du revêtement | Valeurs autorisées : <br>- None (pas de revêtement) <br>- Good (bon état) <br>- Worn (dégradation sans gravité) <br>- Discomfortable (dégradation entraînant une difficulté d'usage ou d'inconfort) <br>- Hazardous (dégradation entraînant un problème de sécurité immédiat) |
| Lighting | éclairage | Valeurs autorisées : <br> - Yes (éclairé et adapté aux déficients visuels) <br> - No (pas d'éclairage) <br> - Bad (éclairé mais non adapté pour les déficients visuels) |
| Tilt | Dévers en pourcentage ou en degré. TODO valeurs négatives autorisées ? | préciser l'unité (% ou deg) dans le champ |
| Slope | Pente en pourcentage ou en degré  TODO valeurs négatives autorisées ?  | préciser l'unité (% ou deg) dans le champ |
| Length | longueur du stationnement | En mètres |
| Width | largeur du stationnement | En mètres |
| VehicleRecharging | indique si une prise pour recharger un véhicule électrique est disponible | Valeurs autorisées : <br> - Yes (oui), <br> - No (non)


# InsideSpace=*

Le tag InsideSpace=* est utilisé sur une ![area picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_area.svg/20px-Osm_element_area.svg.png) zone pour représenter une pièce ou un espace situé à l'intérieur d'un PointOfInterest.

Un InsideSpace doit avoir un ![node picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png) noeud Entrance sur son contour et être à l'intérieur d'une une ![area picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_area.svg/20px-Osm_element_area.svg.png) zone PointOfInterest (il peut partager des ![node picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png) noeuds).

Un InsideSpace peut contenir des SitePathLink et des Amenity.

| Clef | Valeur | Description|
| ---- | ------ | ---------- |
| InsideSpace | Room | une pièce conventionnelle avec murs, une cage d'ascenseur |
| InsideSpace | Area | un espace sans murs, intérieur ou extérieur |
| InsideSpace | Corridor | un couloir, un passage entre des pièces, une cage d'escalier |
| InsideSpace | Quay | un quai de métro, de train, représenté en surfacique. Uniquement à l'intérieur d'un PointOfInterest=StopPlace |

## Tags fréquents

| Clef | Description | Contraintes sur la valeur |
| -------- | -------- | -------- |
| WheelchairAccess | indique si l'access possible en fauteuil roulant | Valeurs autorisées : <br> - Yes (oui), <br> - No (non), <br>- Limited (partiellement, à préciser via le tag WheelchairAccess:Description) |
| AudibleSignals | indique si une signalétique auditive est disponible |Valeurs autorisées : <br> - Yes (oui), <br> - No (non), <br>- Limited (partiellement, à préciser via le tag AudibleSignals:Description) |
| VisualSigns | indique si une signalétique visuelle est disponible |Valeurs autorisées : <br> - Yes (oui), <br> - No (non), <br>- Limited (partiellement, à préciser via le tag VisualSigns:Description) |
| TransportMode | le mode de transport en commun (pour Quay uniquement) | Valeurs autorisées : <br> - Rail (train) <br> - Metro <br> - Tram (tramway) |
| TactileWarningStrip | Présence et état de la bande d'éveil vigilance (pour Quay uniquement) |Valeurs autorisées : <br>- None (pas de BEV) <br>- Good (bon état) <br>- Worn (dégradation sans gravité) <br>- Discomfortable (dégradation entraînant une difficulté d'usage ou d'inconfort) <br>- Hazardous (dégradation entraînant un problème de sécurité immédiat) |
| PublicCode | un identifiant public local (pour Quay uniquement) |  |

# Autres objets

## PathJunction=Yes

Les ![node picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png) noeuds qui font parti d'un SitePathLink peuvent avoir des tags propres :

| Clef                | Description                                                                                                                      | Contraintes sur la valeur    |
| ------------------- | -------------------------------------------------------------------------------------------------------------------------------- | --------- |
| PathJunction | Indique qu'il s'agit d'un noeud de cheminement | valeur fixe à "Yes" |
| Altitude | TODO | |
| KerbHeight | Hauteur du ressaut, à mesurer entre le haut de la bordure du trottoir et la chaussée | en mètres |
| KerbCut:Slope | Pente due à l’inclinaison du trottoir vers la chaussée, dans le cas d'un abaissement de trottoir (bateau) | TODO |
| KerbCut:Width | Distance sur laquelle la hauteur de bordure de trottoir est réduite, dans le cas d'un abaissement de trottoir (bateau) | en mètres |
| TactileWarningStrip | Présence et état de la bande d'éveil vigilance (surface contrastée visuellement et tactilement permettant de signaler un danger) |Valeurs autorisées : <br>- None (pas de BEV) <br>- Good (bon état) <br>- Worn (dégradation sans gravité) <br>- Discomfortable (dégradation entraînant une difficulté d'usage ou d'inconfort) <br>- Hazardous (dégradation entraînant un problème de sécurité immédiat)  |
| TactileWarningStripInstallation | Implantation de la bande d'éveil vigilance |Valeurs autorisées : <br>- Usual <br>- Across (implantée en travers) <br>- Curved (implantée en courbe) <br>- InsufficientWidth (largeur insuffisante) <br>- TooLargeWidth (largeur trop importante) <br>- ShallowDepth (profondeur insuffisante) <br>- NonContrasting (non contrastée) |


## Relations StopPlace=*

On pourra trouver des ![relation picto](https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_relation.svg/20px-Osm_element_relation.svg.png) relations avec un attribut StopPlace.
Il s'agit d'objets abstraits importés lors d'un import Netex et qu'on souhaite conserver lors de l'export Netex. Ils n'ont pas de tag PointOfInterest=StopPlace et il n'est pas nécessaire de renseigner d'information dessus.

Les valeurs les plus fréquentes seront :

| Clef | Valeur | Description |
| -------- | -------- | -------- |
| StopPlace | OnstreetBus | un regroupement d'arrêts de bus proches, situés sur la voirie |
| StopPlace | OnstreetTram | un regroupement d'arrêts de tramway proches, sur la voirie et ne constituant pas une station de tramway |
| StopPlace | FerryStop | un regroupement d'arrêts de ferry proches ne constituant pas une station de ferry |
| StopPlace | Other | un autre regroupement d'arrêts de même mode |

Les relations contiennent comme membres un ou plusieurs objets Quay, sans rôle.

# Tags génériques

Les tags ci-dessous peuvent être trouvés sur n'importe quel type d'objet :
| Clef | Description | Exemple |
| ---- | ----------- | ------- |
| FixMe | une note indiquant que quelque chose est à corriger, à usage interne |         |
| Description | une description à usage du public |  |  |
| VisualSigns:Description | à renseigner obligatoirement et uniquement lorsque VisualSigns = Limited pour expliquer pourquoi la disponibilité n’est que partielle ||
| AudibleSignals:Description | à renseigner obligatoirement et uniquement lorsque AudibleSignals = Limited pour expliquer pourquoi la disponibilité n’est que partielle ||
| WheelchairAccess:Description | à renseigner obligatoirement et uniquement lorsque WheelchairAccess = Limited pour expliquer pourquoi l’accessibilité n’est que partielle | WheelchairAccess:Description = Un bollard empêche le passage en fauteil roulant |
| NonGeographicalLocation | à renseigner à "Yes" sur les éléments en intérieur dont la position exacte n'est pas connue (permet de ne pas exporter la position géographique dans les exports) | NonGeographicalLocation=Yes |
| Ref:Import:Source | indique que l'objet a été créé à l'issue d'un import et la source de celui-ci | Ref:Import:Source=OpenStreetMap |
| Ref:Import:Id | indique l'identifiant de l'objet qui a été importé | Ref:Import:Id=w129174547 |
| Ref:Import:Version | indique la version de l'objet importé, si la source propose du versionnement | Ref:Import:Version=7 |
